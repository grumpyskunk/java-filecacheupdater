# FileCacheUpdater
-----------------

This is a functional proof of concept for a background job which maintains fresh copies of frequently used data files onto a redis instance. This could be used to host an APIs configuration files (such as routes, input validation definitions, etc) and reduce disk IO for high-volume requests.

There are some opportunities for improvement which weren't necessarily called for to demonstrate this concept. Some examples include...
- Implementing a Log4J concept in place of outputting directly to the buffer. This would be done to improve accessibility when reviewing/debugging and also permit log level controls for production environments
- Revising how directory-based caching is evaluated so that the configuration for directory extensions could be a json array. This would permit a single read and iteration of a potentially very large file directory