package com.grumpyskunk.utility;

import io.lettuce.core.RedisClient;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;

import java.util.Map;

public class RedisUtility {

  ConfigUtility                           configUtility;
  RedisClient                             redisClient;
  StatefulRedisConnection<String, String> connection;
  RedisCommands<String, String>           redisCommands;

  /**
   * @param configUtility Config utility class
   */
  public RedisUtility(ConfigUtility configUtility) {
    this.configUtility = configUtility;

    // Initialize our connection to redis
    redisClient   = RedisClient.create("redis://" + configUtility.getRedisAddress());
    connection    = redisClient.connect();
    redisCommands = connection.sync();
  }

  /**
   * Deletes a given key value
   *
   * @param key String key to delete for
   * @return Result of del
   */
  public Long del(String key) {
    return redisCommands.del(key);
  }

  /**
   * Retrieves a given key value
   *
   * @param key String key to get for
   * @return Result of get
   */
  public String get(String key) {
    return redisCommands.get(key);
  }

  /**
   * Deletes from a redis hash map
   *
   * @param hash Hash to delete from
   * @param key  Key to delete for
   * @return Result of hdel
   */
  public Long hdel(String hash, String key) {
    return redisCommands.hdel(hash, key);
  }

  /**
   * Retrieves a value from a redis hash map
   *
   * @param hash Hash to get from
   * @param key  Key to get for
   * @return Result of hget
   */
  public String hget(String hash, String key) {
    return redisCommands.hget(hash, key);
  }

  /**
   * Retrieve all from a redis hash map
   *
   * @param hash Hash to retrieve for
   * @return Resulting hash map
   */
  public Map<String, String> hgetall(String hash) {
    return redisCommands.hgetall(hash);
  }

  /**
   * Sets a redis value for the given hash and key
   *
   * @param hash  Hash to set for
   * @param key   Key to set for
   * @param value Value to set
   * @return True on success - false on failure
   */
  public boolean hset(String hash, String key, String value) {
    return redisCommands.hset(hash, key, value);
  }

  /**
   * Sets a redis value for a given key
   *
   * @param key   Key name
   * @param value Value to store
   * @return String "OK"
   */
  public String set(String key, String value) {
    return redisCommands.set(key, value);
  }

  /**
   * Shuts down our redis client
   */
  public void closeConnection() {
    redisClient.shutdown();
  }

}
