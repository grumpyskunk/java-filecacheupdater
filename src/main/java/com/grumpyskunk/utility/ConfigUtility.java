package com.grumpyskunk.utility;

import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.grumpyskunk.entities.ConfigEntity;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

public class ConfigUtility {

  private final ConfigEntity configEntity;

  /**
   * Instantiate
   *
   * @param configPath  Configuration path string
   * @param fileUtility File utility class
   */
  public ConfigUtility(String configPath, FileUtility fileUtility) throws IOException {
    try {
      Gson gson = new Gson();

      configEntity = gson.fromJson(fileUtility.readFile(configPath), ConfigEntity.class);
      System.out.println("Operating from config definition: " + configEntity.toString());
    } catch (IOException exception) {
      System.out.println("Unable to locate config file. Application will terminate.");

      throw exception;
    }
  }

  public String getAppName() {
    return configEntity.getAppName();
  }

  public String getAppPath() {
    return configEntity.getAppPath();
  }

  public String getCacheKey() {
    return configEntity.getCacheKey();
  }

  public String getConfigPath() {
    return configEntity.getConfigPath();
  }

  public String getRedisAddress() {
    return configEntity.getRedisAddress();
  }

  public int getRedisPort() {
    return configEntity.getRedisPort();
  }

  public int getSleepTime() {
    return configEntity.getSleep();
  }

  public float getVersion() {
    return configEntity.getVersion();
  }


}
