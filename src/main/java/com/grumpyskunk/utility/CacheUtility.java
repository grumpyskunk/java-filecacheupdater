package com.grumpyskunk.utility;

import com.google.gson.Gson;
import com.grumpyskunk.entities.CacheEntity;
import com.grumpyskunk.entities.DirectoryCacheEntity;
import com.grumpyskunk.entities.FileCacheEntity;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CacheUtility {

  CacheEntity         cacheEntity;
  Map<String, String> cachedMap;
  Map<String, Long>   lastModifiedMap;
  ConfigUtility       configUtility;
  FileUtility         fileUtility;
  RedisUtility        redisUtility;

  /**
   * Instantiate
   *
   * @param configUtility Configuration utility class
   * @param fileUtility   File utility class
   * @param redisUtility  Redis utility class
   */
  public CacheUtility(
      ConfigUtility configUtility,
      FileUtility fileUtility,
      RedisUtility redisUtility
  ) {
    try {
      Gson gson = new Gson();

      cacheEntity = gson.fromJson(
          fileUtility.readFile(configUtility.getConfigPath() + "/cache.json"),
          CacheEntity.class
      );
      System.out.println("Operating from cache definition: " + cacheEntity.toString());
    } catch (IOException exception) {
      System.out.println("Unable to locate cache file. Application will terminate.");
    }

    this.configUtility = configUtility;
    this.fileUtility   = fileUtility;
    this.redisUtility  = redisUtility;

    // Initialize cache maps
    this.prepareCacheMaps();
  }

  /**
   * Retrieves all currently cached keys and removes any that are no longer valid
   */
  public void cleanUp() {
    System.out.println("Verifying existing cached objects.");

    // Retrieve all currently cached objects within
    Map<String, String> existing = this.redisUtility.hgetall(this.configUtility.getCacheKey());

    // Compare against files identified valid for cache
    for (Map.Entry<String, String> entry : existing.entrySet()) {
      if (!cachedMap.containsKey(entry.getKey())) {
        System.out.println("Removing deprecated redis data for: " + entry.getKey());
        // Remove stored data from REDIS
        redisUtility.hdel(configUtility.getCacheKey(), entry.getKey());
      }
    }
  }

  /**
   * Instantiates and prepares our cache maps that will be used to track what and when a refresh needs done
   */
  public void prepareCacheMaps() {
    this.cachedMap       = new HashMap<>();
    this.lastModifiedMap = new HashMap<>();

    // Iterate our file cache entity objects and add them to our cached and last updated list
    for (FileCacheEntity fileCacheEntity : cacheEntity.getFiles()) {
      try {
        String fileName = fileUtility.getFilename(fileCacheEntity.getPath());

        // Add cache path
        cachedMap.put(fileName, fileCacheEntity.getPath());
        // Add to last modified map with a zero timestamp to assure a fresh fetch on first load
        lastModifiedMap.put(fileName, 0L);
      } catch (NullPointerException exception) {
        exception.printStackTrace();
      }
    }

    // Iterate our directory cache entity objects...
    for (DirectoryCacheEntity directoryCacheEntity : cacheEntity.getDirectories()) {
      // ...and retrieve the files for the defined extension
      List<String> files = fileUtility.listFilesByExtension(
          directoryCacheEntity.getPath(),
          directoryCacheEntity.getExtension()
      );

      // Iterate the files and add them to our cached and last updated list
      for (String file : files) {
        System.out.println("Initializing for directory file: " + file);
        String fileName = fileUtility.getFilename(file);

        // Add cache path
        cachedMap.put(fileName, file);
        // Add to last modified map with a zero timestamp to assure a fresh fetch on first load
        lastModifiedMap.put(fileName, 0L);
      }
    }
  }

  /**
   * Reconciles the state of any currently cached entries
   */
  public void reconcile() {
    System.out.println("Reconciling cached objects.");

    // Iterate each file identified within our cache map
    for (Map.Entry<String, String> entry : cachedMap.entrySet()) {
      // Retrieve our last known modified timestamp for this entry
      Long lastKnownModified = lastModifiedMap.get(entry.getKey());
      // Retrieve the actual last modified timestamp for this entry
      Long lastActualModified = fileUtility.getLastModified(entry.getValue());

      // If last modified greater than last known modified
      if (lastActualModified > lastKnownModified) {
        try {
          String contents = fileUtility.readFile(entry.getValue());

          redisUtility.hset(configUtility.getCacheKey(), entry.getKey(), contents);
          lastModifiedMap.replace(entry.getKey(), lastActualModified);

          System.out.println("Cache refresh performed for: " + entry.getKey());
        } catch (IOException exception) {
          System.out.println("Exception encountered updating file contents for " + entry.getKey());
          System.out.println(exception.getMessage());
        }
      }
    }
  }

}
