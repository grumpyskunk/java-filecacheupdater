package com.grumpyskunk.utility;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileUtility {

  /**
   * Confirms a file exists and attempts to delete it
   *
   * @param path Filesystem path to delete file from
   * @return boolean
   */
  public boolean deleteFile(String path) {
    File file = new File(path);

    return file.exists() && file.delete();
  }

  /**
   * Retrieves the filename for a given file path
   *
   * @param path Path to file retrieving for
   * @return File name
   */
  public String getFilename(String path) {
    Path filePath = Paths.get(path);

    return filePath.getFileName().toString();
  }

  /**
   * Lists files within a given path for a given extension
   *
   * @param path      Path to file
   * @param extension Extension to filter for
   * @return List of resulting files
   */
  public List<String> listFilesByExtension(String path, String extension) {
    List<String> result = new ArrayList<>();

    try (Stream<Path> walk = Files.walk(Paths.get(path))) {

      result = walk.map(Path::toString).filter(f -> f.endsWith("." + extension)).collect(Collectors.toList());

    } catch (IOException exception) {
      System.out.println(exception.getMessage());
    }

    return result;
  }

  /**
   * Retrieves the last modified timestamp of a given file
   *
   * @param path Path to file
   * @return Last modified value
   */
  public Long getLastModified(String path) {
    File file = new File(path);

    return file.lastModified() / 1000L;
  }

  /**
   * Reads the contents of a file into a single string value
   *
   * @param path Filesystem path to retrieve file from
   * @return String
   */
  public String readFile(String path) throws IOException {
    Path filePath = Paths.get(path);
    return Files.readString(filePath);
  }

}
