package com.grumpyskunk.entities;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ConfigEntity {
  @Expose(serialize = true, deserialize = true)
  @SerializedName(value = "app_name")
  private String            appName;
  @Expose(serialize = true, deserialize = true)
  @SerializedName(value = "app_path")
  private String            appPath;
  @Expose(serialize = true, deserialize = true)
  @SerializedName(value = "cache_key")
  private String            cacheKey;
  @Expose(serialize = true, deserialize = true)
  @SerializedName(value = "config_path")
  private String            configPath;
  @Expose(serialize = true, deserialize = true)
  @SerializedName(value = "redis")
  private RedisClientEntity redisClientEntity;
  @Expose(serialize = true, deserialize = true)
  private int               sleep;
  @Expose(serialize = true, deserialize = true)
  private float             version;

  /**
   * @return Returns app name
   */
  public String getAppName() {
    return appName;
  }

  /**
   * @return Returns app running path
   */
  public String getAppPath() {
    return appPath;
  }

  /**
   * @return Returns app cache key
   */
  public String getCacheKey() {
    return cacheKey;
  }

  /**
   * @return Returns app config path
   */
  public String getConfigPath() {
    return configPath;
  }

  /**
   * @return Returns app redis address
   */
  public String getRedisAddress() {
    return redisClientEntity.getAddress();
  }

  /**
   * @return Returns app redis port
   */
  public int getRedisPort() {
    return redisClientEntity.getPort();
  }

  /**
   * @return Returns app sleep value
   */
  public int getSleep() {
    return sleep;
  }

  /**
   * @return Returns app version
   */
  public float getVersion() {
    return version;
  }

  /**
   * Sets the app name
   *
   * @param appName App name value
   */
  public void setAppName(String appName) {
    this.appName = appName;
  }

  /**
   * Sets the app running path
   *
   * @param appPath App path
   */
  public void setAppPath(String appPath) {
    this.appPath = appPath;
  }

  /**
   * Sets the app cache key value
   *
   * @param cacheKey Cache key value
   */
  public void setCacheKey(String cacheKey) {
    this.cacheKey = cacheKey;
  }

  /**
   * Sets the app configuration path
   *
   * @param configPath Config path value
   */
  public void setConfigPath(String configPath) {
    this.configPath = configPath;
  }

  /**
   * Sets the app threat sleep time
   *
   * @param sleep Sleep value
   */
  public void setSleep(int sleep) {
    this.sleep = sleep;
  }

  /**
   * Sets app version
   *
   * @param version Version value
   */
  public void setVersion(float version) {
    this.version = version;
  }

  @Override
  public String toString() {
    Gson gson = new Gson();

    return gson.toJson(this);
  }
}
