package com.grumpyskunk.entities;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;

public class FileCacheEntity {
  @Expose(serialize = true, deserialize = true)
  private String path;

  /**
   * @return Retrieves cached file path
   */
  public String getPath() {
    return path;
  }

  /**
   * Sets cache file path
   *
   * @param path Path to file to cache
   */
  public void setPath(String path) {
    this.path = path;
  }

  /**
   * Output to string
   */
  @Override
  public String toString() {
    Gson gson = new Gson();

    return gson.toJson(this);
  }
}
