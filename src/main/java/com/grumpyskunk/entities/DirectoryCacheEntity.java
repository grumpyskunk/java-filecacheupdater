package com.grumpyskunk.entities;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;

public class DirectoryCacheEntity {
  @Expose(serialize = true, deserialize = true)
  private String path;
  @Expose(serialize = true, deserialize = true)
  private String extension;

  /**
   * @return Returns cache directory extension filter
   */
  public String getExtension() {
    return extension;
  }

  /**
   * @return Returns cache directory path
   */
  public String getPath() {
    return path;
  }

  /**
   * Sets cache directory extension filter
   *
   * @param extension Extension to filter for
   */
  public void setExtension(String extension) {
    this.extension = extension;
  }

  /**
   * Sets cache directory path
   *
   * @param path Path to cache for
   */
  public void setPath(String path) {
    this.path = path;
  }

  /**
   * Output to string
   */
  @Override
  public String toString() {
    Gson gson = new Gson();

    return gson.toJson(this);
  }
}
