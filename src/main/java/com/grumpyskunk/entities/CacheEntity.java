package com.grumpyskunk.entities;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;

import java.util.List;

public class CacheEntity {

  @Expose(serialize = true, deserialize = true)
  private List<DirectoryCacheEntity> directories;
  @Expose(serialize = true, deserialize = true)
  private List<FileCacheEntity>      files;

  /**
   * @return Returns cached directory definitions
   */
  public List<DirectoryCacheEntity> getDirectories() {
    return directories;
  }

  /**
   * @return Returns cached file definitions
   */
  public List<FileCacheEntity> getFiles() {
    return files;
  }

  /**
   * Sets cached directory definitions
   *
   * @param directories Directory definitions defined within cache.json
   */
  public void setDirectories(List<DirectoryCacheEntity> directories) {
    this.directories = directories;
  }

  /**
   * Sets cached file definitions
   *
   * @param files File definitions defined within cache.json
   */
  public void setFiles(List<FileCacheEntity> files) {
    this.files = files;
  }

  /**
   * Output to string
   */
  @Override
  public String toString() {
    Gson gson = new Gson();

    return gson.toJson(this);
  }

}
