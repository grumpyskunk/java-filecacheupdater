package com.grumpyskunk.entities;

import com.google.gson.annotations.Expose;

public class RedisClientEntity {
  @Expose(serialize = true, deserialize = true)
  private String address;
  @Expose(serialize = true, deserialize = true)
  private int    port;

  /**
   * @return Retrieves redis server address
   */
  public String getAddress() {
    return address;
  }

  /**
   * @return Retrieves redis server port
   */
  public int getPort() {
    return port;
  }

  /**
   * Sets redis server address
   *
   * @param address Address to connect to
   */
  public void setAddress(String address) {
    this.address = address;
  }

  /**
   * Sets redis server port
   *
   * @param port Port to connect to
   */
  public void setPort(int port) {
    this.port = port;
  }
}
