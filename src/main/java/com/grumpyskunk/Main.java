package com.grumpyskunk;

import com.grumpyskunk.utility.CacheUtility;
import com.grumpyskunk.utility.ConfigUtility;
import com.grumpyskunk.utility.FileUtility;
import com.grumpyskunk.utility.RedisUtility;

import java.io.File;

public class Main {

  private static ConfigUtility configUtility;
  private static RedisUtility  redisUtility;

  /**
   * Primary job execution body
   *
   * @param args [0] Configuration file location
   */
  public static void main(String[] args) {
    // If an app config path was not provided...
    if (args.length == 0) {
      System.out.println("Invalid arguments list, please provide path to config file.");
      System.exit(1);
    }

    try {
      FileUtility fileUtility = new FileUtility();

      // Load config utility class
      configUtility = new ConfigUtility(args[0], fileUtility);

      // Load our redis utility class
      RedisUtility redisUtility = new RedisUtility(configUtility);

      // Load our cache utility class
      CacheUtility cacheUtility = new CacheUtility(
          configUtility,
          fileUtility,
          redisUtility
      );

      // Cleanup any existing cached elements
      cacheUtility.cleanUp();

      // While an application stop file does not exist...
      String stopFile = String.format("%s/%s.stop", configUtility.getAppPath(), configUtility.getAppName());
      while (!fileUtility.deleteFile(stopFile)) {
        // Perform cached element reconciliation
        cacheUtility.reconcile();

        // Take a rest
        System.out.println(String.format("Thread resting for: %dms", configUtility.getSleepTime()));
        Thread.sleep(configUtility.getSleepTime());
      }
      // Close our redis connection
      redisUtility.closeConnection();

      System.out.println("Stop file identified. Exiting now.");
    } catch (Exception exception) {
      System.out.println("Unhandled exception occurred. Exiting now.");
      System.out.println(exception.toString());
    }
  }

}
